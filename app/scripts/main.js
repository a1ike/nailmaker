$(document).ready(function() {

  $('.phone').inputmask('+7(999)999-99-99');

  $('.n-card__main').on('click', function(e) {
    e.preventDefault();
    $(this).next($('.n-card__content')).slideToggle('fast');
  });

  $('.open-step-metro-one').on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({
      scrollTop: 0
    }, 'slow');
    $('.n-main').hide('fast');
    $('.n-step-metro-one').addClass('n-step_show');
  });

  $('.open-step-metro-two').on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({
      scrollTop: 0
    }, 'slow');
    $('.n-main').hide('fast');
    $('.n-step-metro-two').addClass('n-step_show');
  });

  $('.open-step-metro-three').on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({
      scrollTop: 0
    }, 'slow');
    $('.n-main').hide('fast');
    $('.n-step-metro-three').addClass('n-step_show');
  });

  $('.open-step-metro-four').on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({
      scrollTop: 0
    }, 'slow');
    $('.n-main').hide('fast');
    $('.n-step-metro-four').addClass('n-step_show');
  });

  $('.open-step-metro-five').on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({
      scrollTop: 0
    }, 'slow');
    $('.n-main').hide('fast');
    $('.n-step-metro-five').addClass('n-step_show');
  });

  $('.open-step-metro-six').on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({
      scrollTop: 0
    }, 'slow');
    $('.n-main').hide('fast');
    $('.n-step-metro-six').addClass('n-step_show');
  });

  $('.open-step-metro-seven').on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({
      scrollTop: 0
    }, 'slow');
    $('.n-main').hide('fast');
    $('.n-step-metro-seven').addClass('n-step_show');
  });

  $('.open-step-metro-eight').on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({
      scrollTop: 0
    }, 'slow');
    $('.n-main').hide('fast');
    $('.n-step-metro-eight').addClass('n-step_show');
  });

  $('.open-step-metro-nine').on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({
      scrollTop: 0
    }, 'slow');
    $('.n-main').hide('fast');
    $('.n-step-metro-nine').addClass('n-step_show');
  });

  $('.open-step-metro-ten').on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({
      scrollTop: 0
    }, 'slow');
    $('.n-main').hide('fast');
    $('.n-step-metro-ten').addClass('n-step_show');
  });

  $('input[name="metro"]').on('input, change', function(e) {
    $('html, body').animate({
      scrollTop: 0
    }, 'slow');
    $('.n-step').removeClass('n-step_show');
    $('.n-step__two').addClass('n-step_show');
  });

  $('.open-step-two').on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({
      scrollTop: 0
    }, 'slow');
    $('.n-main').hide('fast');
    $('.n-step').removeClass('n-step_show');
    $('.n-step__two').addClass('n-step_show');
  });

  $('.n-mob__toggle').on('click', function(e) {
    e.preventDefault();
    $('.n-header').slideToggle('fast');
    $('.n-step__one').addClass('n-step_show');
  });

});